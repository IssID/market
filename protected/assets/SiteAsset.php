<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

class SiteAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'siteAssets/css/bootstrap.min.css',
        'siteAssets/css/classy-nav.min.css',
        'siteAssets/css/owl.carousel.css',
        'siteAssets/css/animate.css',
        'siteAssets/css/magnific-popup.css',
        'siteAssets/css/jquery-ui.min.css',
        'siteAssets/css/nice-select.css',
        'siteAssets/css/font-awesome.min.css',
        'siteAssets/css/core-style.css',
        'siteAssets/css/style.css',
        'siteAssets/css/core-style.css',
        'siteAssets/css/style.css',
    ];
    public $js = [
//        'siteAssets/js/jquery/jquery-2.2.4.min.js',
        'siteAssets/js/popper.min.js',
//        'siteAssets/js/bootstrap.min.js',
        'siteAssets/js/plugins.js',
        'siteAssets/js/classy-nav.min.js',
        'siteAssets/js/active.js',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset', // bootstrap css + js
        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
