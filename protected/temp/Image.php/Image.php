<?php

namespace app\modules\images\models;

use yii\data\Pagination;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $auth_token
 * @property string $access_level
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['link'], 'required'],
            [['single_product_id', 'shop_id', 'product_id', 'users_id'], 'safe'],
        ];
    }
}
