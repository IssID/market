<?php

namespace app\modules\admin\controllers;

use app\models\Category;
use app\models\Image;
use app\models\ProductCategory;
use app\modules\admin\models\CategorySearch;
use app\modules\admin\models\ImageUpload;
use app\models\Product;
//use app\models\Tag;
use app\modules\admin\models\ProductSearch;
use nickdenry\grid\toggle\actions\ToggleAction;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Для переключателя статуса
     * @return array
     */
    public function actions()
    {
        return [
            'toggle' => [
                'class' => ToggleAction::className(),
                'modelClass' => 'app\models\Product', // Your model class
            ],
        ];
    }

    /**
     * Найти модель продукта по ключу
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Список всех Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['pageSize' =>5];

        return $this->render('product/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Показать информацию о продукте
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('product/view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Создать новый продукт
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('product/create', [
            'model' => $model,
        ]);
    }

    /**
     * Обновить информацию продукта
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->category = ArrayHelper::getColumn($model->categores, 'id');
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->saveProduct() && $model->saveCategory($post['Product']['category'])) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('product/update', [
            'model' => $model,
        ]);
    }

    /**
     * Удалить продукт
     * @param integer $id
     * @return mixed
     * NotFoundHttpException if the model cannot be found
     * @throws
     */
    public function actionDelete($id)
    {
        if (($model = Product::findOne($id)) == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        // TODO найти вариант проще
        $categores = ProductCategory::find()->where(['product_id'=>$id])->all();
        foreach ($categores as $category){
            $category->delete();
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Загрузить картинки для продукта
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionSetImage($id)
    {
        // создаем объект модели
        $model = new ImageUpload();

        // если пост запрос
        if(Yii::$app->request->isPost){
            // получаем запись из базы данных по ID
            $product = $this->findModel($id);

            // получаем объекты файлаов из папки /tmp/
            $model->image = UploadedFile::getInstances($model, 'image');

            // перебераем массив
            foreach ($model->image as $file){
                // создаем болванку которая будет заливать в базу
                $image = new Image();

                // создаем уникальное имя для картинки
                $filename = $model->generateFileName($file);

                // загружаем в базу ( название файла)
                if($image->saveImage($filename))
                {
                    // запись в промежуточную таблицу product_images (relation)
                    $image->link('product', $product);
                    // Загружаем картинку на сервер в деректорию приложения
                    $file->saveAs($model->getFolder() . $filename);
                }
            }
            return $this->redirect(['index']);
        }
        return $this->render('image/image', ['model'=>$model]);
    }

    /**
     * Показать картинку продукта
     * @param $id
     * @return string
     */
    public function actionViewImage($id)
    {
        $model = Image::findOne($id);
        return $this->render('image/view', ['model'=>$model]);
    }

    /**
     * Удалить картинку продукта
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelImage($id)
    {
        if (($model = Image::findOne($id)) == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $model->delete();

        // redirect back
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

//    /**
//     * не используется
//     * @param $id
//     * @return string|\yii\web\Response
//     * @throws NotFoundHttpException
//     */
//    public function actionSetCategory($id)
//    {
//        $product = $this->findModel($id);
//        $selectedCategory = $product->getSelectedCategores();
//        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'name');
//
//        if(Yii::$app->request->isPost)
//        {
//            $category = Yii::$app->request->post('category');
//
//            if($product->saveCategory($category))
//            {
//                print_r($category);
//                return $this->redirect(['view', 'id'=>$product->id]);
//            }
//        }
//
//        $searchModel = new CategorySearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
//
//        return $this->render('category', [
//            'searchModel' => $searchModel,
//            'dataProvider' => $dataProvider,
//
//            'product'=>$product,
//            'selectedCategory'=>$selectedCategory,
//            'categories'=>$categories
//        ]);
//    }

    /**
     * Установить и редактировать Категории
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCategory($id='')
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $categories = ArrayHelper::map(Category::find()->all(), 'id', 'name');

        // показываем, если не задан id
        if(!$id) {
            return $this->render('category/index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'categories'=>$categories
            ]);
        }

        $product = $this->findModel($id);
        $selectedCategory = $product->getSelectedCategores();

        // если пришел пост запрос
        if(Yii::$app->request->isPost) {
            $category = Yii::$app->request->post('category');

            if($product->saveCategory($category)) {
                print_r($category);
                return $this->redirect(['view', 'id'=>$product->id]);
            }
        }

        return $this->render('category/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

            'product'=>$product,
            'selectedCategory'=>$selectedCategory,
            'categories'=>$categories
        ]);

    }

    /**
     * Создать категорию
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateCategory()
    {
        $model = new Category();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/admin/product/category']);
        }

        return $this->render('category/create', [
            'model' => $model,
        ]);
    }

    /**
     * Редактировать категорию
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdateCategory($id)
    {
        if (($model = Category::findOne($id)) == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            return $this->redirect(['/admin/product/category']);
        }

        return $this->render('category/update', [
            'model' => $model,
        ]);
    }

    /**
     * Удалить категорию
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelCategory($id)
    {
        if (($model = Category::findOne($id)) == null) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $model->delete();

        return $this->redirect(['/admin/product/category']);
    }
}
