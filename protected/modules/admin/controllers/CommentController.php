<?php
/**
 * Created by PhpStorm.
 * User: IssID
 * Date: 05.08.2018
 * Time: 2:08
 */

namespace app\modules\admin\controllers;


use app\models\Comment;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CommentController extends Controller
{
    public function actionIndex()
    {
        $comments = Comment::find()->orderBy('id desc')->all();

        return $this->render('index',[
           'comments' => $comments,
        ]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        // проверяем уровень доступа
        $user = Yii::$app->user->identity;
        if (isset($user->access_token) && $user->access_token >= 99) {
            $comment = Comment::findOne($id);
            if ($comment->delete()) {
                return $this->redirect(['comment/index']);
            }
        }else{
            throw new NotFoundHttpException();
        }
    }

    public function actionAllow($id)
    {
        $comment = Comment::findOne($id);
        if ($comment->allow())
        {
            return $this->redirect(['index']);
        }
    }

    public function actionDisallow($id)
    {
        $comment = Comment::findOne($id);
        if ($comment->disallow())
        {
            return $this->redirect(['index']);
        }
    }
}