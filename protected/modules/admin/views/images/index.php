<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ImageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Images';
$this->params['breadcrumbs'][] = ['label' => 'admin', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-comment mr0">
    <div class="category-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Загрузить', ['upload'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'attribute' => 'img',
                    'format' => 'html',
                    'value' => function($data,$url){
                        return Html::a(Html::img($data->getImage(),['width'=>100]), '/admin/images/view?id='.$url);
                    }
                ],
                'image',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{delete}',
                    'buttons' => [
                        'delete' => function ($url,$model,$key) {
                            return Html::a('Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ;
//                            return Html::a('<span class="glyphicon glyphicon-screenshot"></span>', $url);
                        },
                    ],
                ],
//                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
