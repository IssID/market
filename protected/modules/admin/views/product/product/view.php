<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use nickdenry\grid\toggle\components\RoundSwitchColumn;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'admin', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-comment mr0">
    <div class="article-view">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Set Image', ['set-image', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
            <?= Html::a('Set Category', ['set-category', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
            <?=  Html::a('Set Tags', ['set-tags', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                'description:ntext',
                'price',
                [
                    'format' => 'html',
                    'label' => 'category',
                    'value' => function($data){
                        $categoryName = '';
                        foreach ($data->categores as $category){
                            $categoryName =
                                Html::a(
                                        $category->name,
                                        '/admin/category/view?id='.$category->id).
                                ",<br>".
                                $categoryName;
                        }
                        return $categoryName ; //Html::a($categoryName, '/admin/category');
                    }
                ],
                [
                    'attribute' => 'images',
                    'format' => 'html',
                    'value' => function($data){
                        $images= '';
                        foreach ($data->images as $image){
                           $images = $images .
                               Html::a(
                                   Html::img('/uploads/'.$image->image,['width'=>100, 'class'=>'productImg']),
                                   '/admin/images/view?id='.$image->id
                               );
                        }
                        return $images;
                    }
                ],
                'discount',
                [
                    'class' => RoundSwitchColumn::className(),
                    'attribute' => 'status',
                    /* other column options, i.e. */
                    'headerOptions' => ['width' => 150],
                ],
            ],
        ]) ?>

    </div>
</div>
