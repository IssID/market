<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Image */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Images';
$this->params['breadcrumbs'][] = ['label' => 'admin', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'product', 'url' => ['/admin/product']];
$this->params['breadcrumbs'][] = $this->title;

if (!$model){
    return Yii::$app->response->redirect(Url::to(['/admin/product']));
    throw new NotFoundHttpException('The requested page does not exist.');
}
?>
<div class="leave-comment mr0">
    <div class="article-form">

        <?= Html::a('Delete', ['del-image', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <br>
        <br>
            <?= Html::img($model->getImage(), ['max-width'=>'100%']) ?>

    </div>
</div>
