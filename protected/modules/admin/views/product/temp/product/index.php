<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Product;

use nickdenry\grid\toggle\components\RoundSwitchColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @throws */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = ['label' => 'admin', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="leave-comment ">
    <div class="article-index">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('Создать продукт', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('reset', [''], ['class' => 'btn btn-warning']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'attribute' => 'name',
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var Product $model */
                        return Html::a(
                            $model->name,
                            ['view', 'id' => $model->id]
                        );
                    }
                ],
//                'name',
                [
                    'attribute' => 'description',
                    'format' => 'raw',
                    'headerOptions' => ['class' => 'col-sm-6'],
                    'value' => function ($model) {
                        /** @var Product $model */
                        return Html::a(
                            $model->description,
                            ['view', 'id' => $model->id]
                        );
                    }
                ],
                [
                    'attribute' => 'price',
                    'format' => 'raw',
                    'value' => function ($model) {
                        /** @var Product $model */
                        return Html::a(
                            number_format((float)$model->price, 2, '.', ''),
                            ['view', 'id' => $model->id]
                        );
                    }
                ],
                [
                    'attribute' => 'category',
                    'filter' => \app\models\Category::getParentsList(),
                    'format' => 'html',
                    'content' => function($data){
                        $categoryName = Html::a('Category', ['category', 'id' => $data->id], ['class' => 'btn btn-default']);
                            foreach ($data->categores as $category){
                                $categoryName = $categoryName ."<br>". Html::a($category->name, '/admin/category/view?id='.$category->id);
                            }
                        return Html::a($categoryName, '/admin/category');
                    }
                ],
//                [
//                    'attribute' => 'images',
//                    'format' => 'html',
//                    'headerOptions' => ['class' => 'col-sm-1'],
//                    'value' => function($data){
//                        return  Html::a('Set Image', ['set-image', 'id' => $data->id], ['class' => 'btn btn-default']);
//                    }
//                ],
                [
                    'attribute' => 'images',
                    'format' => 'html',
                    'headerOptions' => ['class' => 'col-sm-4'],
                    'value' => function($data){
                        $images= Html::a('Set Image', ['set-image', 'id' => $data->id], ['class' => 'btn btn-default', 'width'=>100])."<br>";
                        foreach ($data->images as $image){
                            $images = $images .
                                Html::a(
                                    Html::img('/uploads/'.$image->image,['width'=>100, 'class'=>'productImg']),
                                    '/admin/images/view?id='.$image->id
                                );
                        }
                        return $images;
                    }
                ],
                'discount',
                [
                    'class' => RoundSwitchColumn::className(),
                    'attribute' => 'status',
                    /* other column options, i.e. */
                    'headerOptions' => ['width' => 150],
                ],
//                [
//                    /**
//                     * Название поля модели
//                     */
//                    'attribute' => 'active',
//                    /**
//                     * Формат вывода.
//                     * В этом случае мы отображает данные, как передали.
//                     * По умолчанию все данные прогоняются через Html::encode()
//                     */
//                    'format' => 'raw',
//                    /**
//                     * Переопределяем отображение фильтра.
//                     * Задаем выпадающий список с заданными значениями вместо поля для ввода
//                     */
//                    'filter' => [
//                        0 => 'No',
//                        1 => 'Yes',
//                    ],
//                    /**
//                     * Переопределяем отображение самих данных.
//                     * Вместо 1 или 0 выводим Yes или No соответственно.
//                     * Попутно оборачиваем результат в span с нужным классом
//                     */
//                    'value' => function ($model, $key, $index, $column) {
//                        $active = $model->{$column->attribute} === 1;
//                        return \yii\helpers\Html::tag(
//                            'span',
//                            $active ? 'Yes' : 'No',
//                            [
//                                'class' => 'label label-' . ($active ? 'success' : 'danger'),
//                            ]
//                        );
//                    },
//                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
