<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use nickdenry\grid\toggle\components\RoundSwitchColumn;
/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'price')->textInput(['type' => 'integer']) ?>
<?php if (Yii::$app->controller->action->id != 'create'): ?>

    <?= $form->field($model, 'category')->dropDownList( $model->getCategoryList(), ['multiple'=>'multiple']) ?>
    <?= $form->field($model, 'discount')->textInput(['type' => 'integer']) ?>
    <?= $form->field($model, 'status')->dropDownList([0 => 'off', 1 => 'on' ] ) ?>



<?php endif; ?>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
