<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

$this->title = 'Categories';
$this->params['breadcrumbs'][] = ['label' => 'admin', 'url' => ['/admin']];
$this->params['breadcrumbs'][] = ['label' => 'product', 'url' => ['/admin/product']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="leave-comment mr0">
    <?php if (isset($product)): ?>
    <div class="article-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= Html::dropDownList('category', $selectedCategory, $categories, ['class'=>'form-control', 'multiple'=>true, 'size'=>10]) ?>

        <div class="form-group"><br>
            <?= Html::submitButton('Upload', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>


    </div>
    <?php endif; ?>
    <div class="article-form">

        <h1><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('Create Category', ['/admin/product/create-category'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//                'id',
                'name',
                [
                    'format' => 'raw',
                    'value' => function ($model) use ($categories) {
                        /** @var Product $model */
                        return  Html::a(
                            'Update',
                            '/admin/product/update-category?id='.$model->id,
//                                ['update', 'id' => $model->id],
                                ['class' => 'btn btn-primary']
                            )." ".
                                Html::a(
                                'Delete',
                                '/admin/product/del-category?id='.$model->id,
//                                    ['delete', 'id' => $model->id],
                                    [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);
                        }
                ]
//                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>