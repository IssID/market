<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

// TODO проверить какие используются функции. ненужные удалить.
/**
 * Class ImageUpload
 * @package app\modules\admin\models
 */
class ImageUpload extends Model
{
    /** @var array $image*/
    public $image;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions' => 'jpg,png', 'maxFiles' => 10],
        ];
    }

    /**
     * возможно уже не используется
     *
     * @param UploadedFile $file
     * @param $currentImage
     * @return string
     */
    public function uploadFile(UploadedFile $file, $currentImage)
    {
        $this->image = $file;

        if ($this->validate()) {
            $this->deleteCurrentImage($currentImage);
            // возращаем имя
            return $this->saveImage();
        }
    }

    /**
     * Получить папку в которой будем хранить файлы
     *
     * @return string
     */
    public function getFolder()
    {
        return Yii::getAlias('@web') . 'uploads/';
    }

    /**
     * Генерируем уникальное название для картинки
     *
     * @param string $file
     * @return string
     */
    public function generateFileName($file = '')
    {
        return $filename = strtolower(md5(uniqid($file->baseName))) . "." . $file->extension;
    }

    /**
     * @param $currentImage
     */
    public function deleteCurrentImage($currentImage)
    {
        if ($this->fileExists($currentImage)) {
            // удаляем старый файл
            unlink($this->getFolder() . $currentImage);
        }
    }

    /**
     * @param $currentImage
     * @return bool
     */
    public function fileExists($currentImage)
    {
        // проверяем на наличие записи в базе и наличии файла в директории
        if (!empty($currentImage) && $currentImage != null) {
            return is_file($this->getFolder() . $currentImage);
        }
    }

    /**
     * @return string
     */
    public function saveImage()
    {
        // создаем уникальное имя для картинки
        $filename = $this->generateFileName();

        // Загружаем картинку на сервер в деректорию приложения
        $this->image->saveAs($this->getFolder() . $filename);

        return $filename;
    }
}