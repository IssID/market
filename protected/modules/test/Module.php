<?php

namespace app\modules\test;

use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * test module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\test\controllers';
//    public $layout = '/test';


    /**
     * прячем модуль полностью
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new NotFoundHttpException(); // выводим ошибку авторизации и 404
                },
                'rules' => [
                    [
                        'allow' => true,
                        'matchCallback' => function ($rule, $action) {
                            $user = Yii::$app->user->identity;

                            // TODO нужно задавать access_token и уровень доступа |
                            // TODO if (isset($user->access_token) && $user->access_token >= 99) {
                            if (!Yii::$app->user->isGuest && $user->access_level >= 99) {
                                return true;
                            }
                            return false;
                        }
                    ]
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
