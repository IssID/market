<?php

namespace app\modules\test\controllers;

use app\models\Category;
use app\models\Product;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        $products = Product::find()->with('categores')->all();
        $categores = Category::find()->with('products')->all();


        return $this->render('index', ['products' => $products, 'categores' => $categores]);
    }

}
