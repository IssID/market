<?php

namespace app\models;

use yii\data\Pagination;
use yii\helpers\ArrayHelper;

/**
 * Class Product
 * @package app\models
 */
class Product extends \yii\db\ActiveRecord
{
    public $category;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','description', 'price'], 'required'],
            [['discount'], 'integer'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
            'price' => 'Цена',
            'category' => 'Категория',
        ];
    }

    /**
     * добавляем id пользователя и сохраняем
     * @return bool
     */
    public function saveProduct()
    {
        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategores()
    {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])
            ->viaTable('product_category', ['product_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getCategoryList()
    {
        return ArrayHelper::map(Category::find()->all(), 'id', 'name');
    }

    /**
     * @return array
     */
    public function getSelectedCategores()
    {
        $selectedCategores = $this->getCategores()->select('id')->asArray()->all(); //получаем массив массивов
        return ArrayHelper::getColumn($selectedCategores, 'id'); // преобразуем в массив данных
    }

    /**
     * @param $categores
     * @return bool
     */
    public function saveCategory($categores)
    {
        if (is_array($categores)) {
            $this->clearCurrentCategores();

            foreach ($categores as $category_id) {
                $category = Category::findOne($category_id);
                $this->link('categores', $category);
            }
            return true;
        }
        return false;
    }

    /**
     * @return int
     */
    public function clearCurrentCategores()
    {
        return ProductCategory::deleteAll(['product_id' => $this->id]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(Image::className(), ['id' => 'images_id'])
            ->viaTable('product_images', ['product_id' => 'id']);
    }

    /**
     * получаем все записи Product и включаем пагинацию
     *
     * @param int $pageSize
     * @return mixed
     */
    public static function getAll($pageSize = 5)
    {
        // получаем модель
        $query = Product::find();
        // собственно сама пагинация (полное количество объектов, сколько выводить на страницу)
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => $pageSize]);

        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        $data['products'] = $products;
        $data['pages'] = $pages;

        return $data;
    }

//    public static function getProducts($pageSize = 5)
//    {
//        // получаем модель
//        $query = Product::find()->where(['status' => 1]);
//        // собственно сама пагинация (полное количество объектов, сколько выводить на страницу)
//        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => $pageSize]);
//
//        $products = $query->offset($pages->offset)
//            ->limit($pages->limit)
//            ->all();
//        $data['products'] = $products;
//        $data['pages'] = $pages;
//
//        return $data;
//    }

    /**
     * @return string
     */
    public function getImage()
    {
//
//        echo "<pre>";
//        print_r($this->image);
//        //echo "</pre>";
//        exit;
        return ($this->image) ? '/uploads/' . $this->photo : '/no-image.png';
    }
}
