<?php

namespace app\models;

use yii\base\Model;

/**
 * Class SignupForm
 * @package app\models
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $confirmPassword;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'email', 'password', 'confirmPassword'], 'required'],
            [['username'], 'string'],
            [['email'], 'email'],
            ['confirmPassword', 'compare', 'compareAttribute' => 'password'],     // сравниваем второй пароль с первым
            [['email'], 'unique', 'targetClass' => 'app\models\User', 'targetAttribute' => 'email']
        ];
    }

    /**
     * @return bool
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $this->password = md5($this->password); // шифруем пароль
            $user->attributes = $this->attributes;
            return $user->create();
        }
    }
}