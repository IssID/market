<?php

namespace app\models;

use app\models\Product;
use yii\base\Model;
use yii\data\ActiveDataProvider;


/**
 * ProductSearch represents the model behind the search form of `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'description', 'price','discount','status'], 'safe'],
            [['category'], 'safe'], // показать поле поиска (фильтрации)
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // получаем олько активированные продукты
        $query = Product::find()->where(['status' => 1]);

        // поиск по связаным таблицам
        $query->joinWith(['categores']);

        // правильный подсчет для пагинации
        $query->distinct();

        // включаем дата провайдер
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // загружаем параметры
        $this->load($params);

        // проверяем валидацию
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        /** фильтры */
        // grid filtering conditions

        // по id продукта
        $query->andFilterWhere([
            'product.id' => $this->id,
        ]);

        // по id категории
        $query->andFilterWhere([
            'category.id' => $this->category,
        ]);

//        $query->andFilterWhere(['like', 'product.name', $this->name]);
//        $query->andFilterWhere(['like', 'product.description', $this->description]);
//        $query->andFilterWhere(['like', 'product.price', $this->price]);
//        $query->andFilterWhere(['like', 'category.id', $this->category]);
//        $query->andFilterWhere(['like', 'product.discount', $this->discount]);
//        $query->andFilterWhere(['like', 'product.status', $this->status]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getCategoryList()
    {
        return parent::getCategoryList();
    }
}
