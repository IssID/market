<?php

namespace app\models;

use app\modules\admin\models\ImageUpload;
use yii\helpers\ArrayHelper;

/**
 * Class Image
 * @package app\models
 */
class Image extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Картинка',
            'image' => 'Путь / Название',
        ];
    }

    /**
     * Сохраняем запись в базу
     *
     * @param $uploadFile
     * @return bool
     */
    public function saveImage($uploadFile)
    {
        $this->image = $uploadFile;
        return $this->save(false);
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }

    /**
     * Delete Image
     */
    public function deleteImage()
    {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->image);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->deleteImage();
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('product_images', ['images_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getImagesList()
    {
        return ArrayHelper::map(Image::find()->all(), 'id', 'image');
    }

}
