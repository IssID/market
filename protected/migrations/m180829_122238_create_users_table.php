<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m180829_122238_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'email' => $this->char(64),
            'username' => $this->char(64),
            'password' => $this->char(64),
            'first_name' => $this->char(64),
            'last_name' => $this->char(64),
            'access_level' => $this->char(64),
            'auth_token' => $this->char(64),
            'birthday' => $this->date(),
        ]);

        $this->insert('users', [
            'id' => 1,
            'email' => 'iccid_zxz@mail.ru',
            'username' => 'IssID',
            'password' => '3620eaa3c097e3a9f02d8d507854247f',
            'access_level' => 100,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
