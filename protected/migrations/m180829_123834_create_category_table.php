<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m180829_123834_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'name' => $this->char(64),
        ]);

        $this->insert('category', [
            'id' => 1,
            'name' => 'канцелярия',
        ]);
        $this->insert('category', [
            'id' => 2,
            'name' => 'tests',
        ]);
        $this->insert('category', [
            'id' => 3,
            'name' => 'new cata',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}