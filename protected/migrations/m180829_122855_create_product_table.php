<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m180829_122855_create_product_table extends Migration
{
    /**
     * {@inheritdoc}
     * @throws \yii\base\Exception
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'name' => $this->char(64),
            'description' => $this->text(),
            'price' => $this->float(),
            'discount' => $this->integer(5),
            'status' => $this->boolean()->defaultValue(0),
        ]);

        $this->insert('product', [
                'id' => 1,
                'name' => 'карандаш',
                'description' => 'хороший карандаш',
                'price' => '5.3',
            ]);
        $this->insert('product', [
                'id' => 2,
                'name' => 'ручка',
                'description' => 'простая шариковая ручка',
                'price' => '7.2',
            ]);
        $this->insert('product', [
                'id' => 3,
                'name' => 'ластик',
                'description' => 'синий ластик с рисунком !',
                'price' => '3.4',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
