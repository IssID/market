<?php

use yii\db\Migration;

/**
 * Handles the creation of table `images`.
 */
class m180830_040946_create_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('images', [
            'id' => $this->primaryKey(),
            'image' => $this->char(64),
        ]);

        $this->insert('images', [
            'id' => 1,
            'image' => 'ef055c56ae6e730ea8d94e156af18695.png',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('images');
    }
}
