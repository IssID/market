<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_images`.
 * Has foreign keys to the tables:
 *
 * - `product`
 * - `images`
 */
class m180829_120939_create_junction_table_for_product_and_images_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_images', [
            'product_id' => $this->integer(),
            'images_id' => $this->integer(),
            'PRIMARY KEY(product_id, images_id)',
        ]);

        // creates index for column `product_id`
        $this->createIndex(
            'idx-product_images-product_id',
            'product_images',
            'product_id'
        );

        // add foreign key for table `product`
        $this->addForeignKey(
            'fk-product_images-product_id',
            'product_images',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );

        // creates index for column `images_id`
        $this->createIndex(
            'idx-product_images-images_id',
            'product_images',
            'images_id'
        );

        // add foreign key for table `images`
        $this->addForeignKey(
            'fk-product_images-images_id',
            'product_images',
            'images_id',
            'images',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `product`
        $this->dropForeignKey(
            'fk-product_images-product_id',
            'product_images'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx-product_images-product_id',
            'product_images'
        );

        // drops foreign key for table `images`
        $this->dropForeignKey(
            'fk-product_images-images_id',
            'product_images'
        );

        // drops index for column `images_id`
        $this->dropIndex(
            'idx-product_images-images_id',
            'product_images'
        );

        $this->dropTable('product_images');
    }
}
