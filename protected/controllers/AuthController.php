<?php
namespace app\controllers;

use app\models\SignupForm;
use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;

class AuthController extends Controller
{
    /**
     * Регистрация
     * @return string|Response
     */
    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            if ($post['SignupForm']['confirmPassword'] == $post['SignupForm']['password']) {
                $model->load($post);
                if ($model->signup()) {
                    return $this->redirect(['auth/login']);
                }
            } else {
                echo "Пароли не совпадают";
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Авторизация
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Разлогиниться
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}