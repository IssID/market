<?php

namespace app\controllers;

use app\models\Category;
use app\models\Product;
use app\models\ProductSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class SiteController extends Controller
{
    public static function  getVersion()
    {
        return '0.003';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout',],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Показать главную страницу
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionShop()
    {
        // форма поиска объектов
        $searchModel = new ProductSearch();
        // создаем датапровайдер
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // задаем количество элементов на странице
        $dataProvider->pagination = ['pageSize' => 3];

        //@todo возможно не потребуется
        // получаем все категории
        $categories = Category::find()->all();

        return $this->render('shop', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'pages' => $dataProvider->pagination,
            'categories' => $categories,
        ]);

    }
    public function actionShopTest()
    {
        return $this->render('temp/shop');

    }

    public function actionProductDetails()
    {
        return $this->render('product-details');
    }

    public function actionCheckout()
    {
        return $this->render('checkout');
    }

    public function actionBlog()
    {
        return $this->render('blog');
    }

    public function actionSingleBlog()
    {
        return $this->render('single-blog');
    }

    public function actionRegularPage()
    {
        return $this->render('regular-page');
    }

    public function actionContact()
    {
        return $this->render('contact');
    }

    public function actionPerson()
    {
        $user = Yii::$app->user->identity;
        if (isset($user->access_level) && $user->access_level >= 99) {
            return $this->render('person', ['user' => $user]);
        }else{
            throw new NotFoundHttpException();
        }
    }
}
