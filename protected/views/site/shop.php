<?php

use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<!-- ##### Breadcumb Area Start ##### -->
<div class="breadcumb_area bg-img" style="background-image: url(/siteAssets/img/bg-img/breadcumb.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="page-title text-center">
                    <h2>dresses</h2>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Breadcumb Area End ##### -->

<!-- ##### Shop Grid Area Start ##### -->
<section class="shop_grid_area section-padding-80">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
                <div class="shop_sidebar_area">

                    <!-- ##### Single Widget ##### -->
                    <div class="widget catagory mb-50">
                        <h6 class="widget-title mb-30">свои фильтры</h6>
                        <?= $this->render('_search', ['model' => $searchModel]) ?>
                        <br>
                        <br>
                        <!-- Widget Title -->
                        <h6 class="widget-title mb-30">Categories</h6>

                        <!--  Categories  -->
                        <div class="categories-menu">
                            <ul id="menu-content2" class="menu-content collapse show">
                                <div class="post-search">

                                <?php // @todo возможно не потребуется ?>
                                <?php foreach ($categories as $category): ?>
                                    <a href="<?= Url::to(['shop', 'category'=> $category->id]) ?>"><?= $category->name ?></a><br>
                                <?php endforeach; ?>

<!--                                <li data-toggle="collapse" data-target="#clothing">-->
<!--                                    <a href="#">clothing</a>-->
<!--                                    <ul class="sub-menu collapse show" id="clothing">-->
<!--                                        <li><a href="?=all">All</a></li>-->
<!--                                        <li><a href="#">Bodysuits</a></li>-->
<!--                                        <li><a href="#">Dresses</a></li>-->
<!--                                        <li><a href="#">Hoodies &amp; Sweats</a></li>-->
<!--                                        <li><a href="#">Jackets &amp; Coats</a></li>-->
<!--                                        <li><a href="#">Jeans</a></li>-->
<!--                                        <li><a href="#">Pants &amp; Leggings</a></li>-->
<!--                                        <li><a href="#">Rompers &amp; Jumpsuits</a></li>-->
<!--                                        <li><a href="#">Shirts &amp; Blouses</a></li>-->
<!--                                        <li><a href="#">Shirts</a></li>-->
<!--                                        <li><a href="#">Sweaters &amp; Knits</a></li>-->
<!--                                    </ul>-->
<!--                                </li>-->

                            </ul>
                        </div>
                    </div>

                    <!-- ##### Single Widget ##### -->
                    <div class="widget price mb-50">
                        <!-- Widget Title -->
                        <h6 class="widget-title mb-30">Filter by</h6>
                        <!-- Widget Title 2 -->
                        <p class="widget-title2 mb-30">Price</p>

                        <div class="widget-desc">
                            <div class="slider-range">
                                <div data-min="49" data-max="360" data-unit="$" class="slider-range-price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" data-value-min="49" data-value-max="360" data-label-result="Range:">
                                    <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span>
                                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span>
                                </div>
                                <div class="range-price">Range: $49.00 - $360.00</div>
                            </div>
                        </div>
                    </div>

                    <!-- ##### Single Widget ##### -->
                    <div class="widget color mb-50">
                        <!-- Widget Title 2 -->
                        <p class="widget-title2 mb-30">Color</p>
                        <div class="widget-desc">
                            <ul class="d-flex">
                                <li><a href="#" class="color1"></a></li>
                                <li><a href="#" class="color2"></a></li>
                                <li><a href="#" class="color3"></a></li>
                                <li><a href="#" class="color4"></a></li>
                                <li><a href="#" class="color5"></a></li>
                                <li><a href="#" class="color6"></a></li>
                                <li><a href="#" class="color7"></a></li>
                                <li><a href="#" class="color8"></a></li>
                                <li><a href="#" class="color9"></a></li>
                                <li><a href="#" class="color10"></a></li>
                            </ul>
                        </div>
                    </div>

                    <!-- ##### Single Widget ##### -->
                    <div class="widget brands mb-50">
                        <!-- Widget Title 2 -->
                        <p class="widget-title2 mb-30">Brands</p>
                        <div class="widget-desc">
                            <ul>
                                <li><a href="#">Asos</a></li>
                                <li><a href="#">Mango</a></li>
                                <li><a href="#">River Island</a></li>
                                <li><a href="#">Topshop</a></li>
                                <li><a href="#">Zara</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-8 col-lg-9">
                <!-- Удалить ссылку -->
<!--                <center><a  href="shop-test"><h1>-> Пример </h1></a></center>-->
                <div class="shop_grid_product_area">
                    <div class="row">
                        <div class="col-12">
                            <div class="product-topbar d-flex align-items-center justify-content-between">
                                <!-- Total Products -->
                                <div class="total-products">
                                    <p><span><?= $dataProvider->getTotalCount() ?></span> products found</p>
                                </div>
                                <!-- Sorting -->
                                <div class="product-sorting d-flex">
                                    <p>Sort by:</p>
                                    <form action="#" method="get">
                                        <select name="select" id="sortByselect">
                                            <option value="value">Highest Rated</option>
                                            <option value="value">Newest</option>
                                            <option value="value">Price: $$ - $</option>
                                            <option value="value">Price: $ - $$</option>
                                        </select>
                                        <input type="submit" class="d-none" value="">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <?php foreach ($dataProvider->getModels() as $item):?>
                            <!-- Single Product -->
                            <div class="col-12 col-sm-6 col-lg-4">
                                <div class="single-product-wrapper">
                                    <!-- Product Image -->
                                    <div class="product-img">
                                        <a href="/product-details?id=<?= $item->id ?>">
                                            <?php $image = isset($item->images[0]) ? $item->images[0]->image : null; ?>
                                            <img class="imageProduct" src="/uploads/<?= $image ?>" alt="">
                                            <!-- Hover Thumb -->
<!--                                            <img class="imageProduct hover-img" src="/uploads/--><?php ////echo $item->images[0]->image ?><!--" alt="">-->
                                        </a>
                                        <!-- Product Badge -->
                                        <?php if(isset($item->discount)): ?>
                                            <div class="product-badge offer-badge">
                                                <span>-<?= $item->discount ?>%</span>
                                            </div>
                                        <?php elseif (is_int($item)): ?>
                                            <!-- Product Badge -->
                                            <div class="product-badge new-badge">
                                                <span>New</span>
                                            </div>
                                        <?php endif; ?>

                                        <!-- Favourite -->
                                        <div class="product-favourite">
                                            <a href="#" class="favme fa fa-heart"></a>
                                        </div>
                                    </div>

                                    <!-- Product Description -->
                                    <div class="product-description">
                                        <span>topshop</span>
                                        <a href="/product-details">
                                            <h6><?= $item->name ?></h6>
                                        </a>
                                        <?php if(is_int($item)): ?>
                                            <p class="product-price"><span class="old-price">$75.00</span> $55.00</p>
                                        <?php else: ?>
                                            <p class="product-price"><?= sprintf("%.2f" . "р", $item->price) ?></p>
                                        <?php endif; ?>


                                        <!-- Hover Content -->
                                        <div class="hover-content">
                                            <!-- Add to Cart -->
                                            <div class="add-to-cart-btn">
                                                <a href="#" class="btn essence-btn">Add to Cart</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <nav aria-label="navigation">
                        <?php

                        // подключаем пагинацию
                        echo LinkPager::widget([
                            'pagination' => $pages,
                        ]); ?>
                </nav>



            </div>
        </div>
    </div>
</section>
<!-- ##### Shop Grid Area End ##### -->
