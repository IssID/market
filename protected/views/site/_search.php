<?php
use \yii\bootstrap\ActiveForm;
use \yii\helpers\Html;

?>
<div class="post-search">
    <?php $form = ActiveForm::begin([
        'action' => ['shop'],
        'method' => 'get',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    <div class="col-lg-10">
        <div class="form-group">
            <?= $form->field($model, 'name') ?>
            <?= $form->field($model, 'category')
                ->dropDownList(
                    $model->getCategoryList(),
                    [
                        'prompt' => ' - ',
                        'class' => 'col-lg-1 dropdown-width-fix',
                    ]
                ) ?>
        </div>
    </div>
    <div class="widget price mb-50">
        <!-- Widget Title -->
        <h6 class="widget-title mb-30">Filter by</h6>
        <!-- Widget Title 2 -->
        <p class="widget-title2 mb-30">Price</p>

        <div class="widget-desc">
            <div class="slider-range">
                <div data-min="49" data-max="360" data-unit="$" class="slider-range-price ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all" data-value-min="49" data-value-max="360" data-label-result="Range:">
                    <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span>
                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0"></span>
                </div>
                <div class="range-price">Range: $49.00 - $360.00</div>
            </div>
        </div>
    </div>

    <div class="form-group col-lg-12">
        <?= Html::submitButton('Искать', ['class' => 'btn btn-primary']) ?>
        <?php // Html::submitButton('Сбросить', ['class' => 'btn btn-default']) ?>
        <?= Html::a('reset', [''], ['class' => 'btn btn-warning']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
