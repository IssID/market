<?php
use yii\helpers\Html;
?>
<div class="leave-comment mr0"><!--leave comment-->
    <div class="container">
        <div clss="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="site-login">
                    <h1><?= \yii\helpers\Html::encode($this->title) ?></h1>
                    <?php if (isset($user->access_level) && $user->access_level >= 99): ?>
                    <?= Html::a('admin'.$this->title, ['admin'], ['class' => 'btn btn-success']) ?>
                    <?php endif; ?>
                    <p>
                        <?= $user->first_name ?><br>
                        <?= $user->last_name ?><br>
                        <?= $user->birthday ?><br>
                        <?= $user->email ?><br>
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>


