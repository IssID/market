<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\SiteAsset;
use app\assets\AdminAsset;
use yii\helpers\Url;

AdminAsset::register($this);


$this->title = 'Essence - Fashion Ecommerce Template';

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/siteAssets/img/core-img/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<!-- ##### Header Area Start ##### -->
<header class="header_area">
    <div class="classy-nav-container breakpoint-off d-flex align-items-center justify-content-between">
        <!-- Classy Menu -->
        <nav class="classy-navbar" id="essenceNav">
            <!-- Logo -->
            <a class="nav-brand" href="/"><img src="/siteAssets/img/core-img/logo.png" alt=""></a>
            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
                <span class="navbarToggler"><span></span><span></span><span></span></span>
            </div>
            <!-- Menu -->
            <div class="classy-menu">
                <!-- close btn -->
                <div class="classycloseIcon">
                    <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                </div>
                <!-- Nav Start -->
                <div class="classynav">
                    <ul>
<!--                        <li><a href="#">Shop</a>-->
<!--                            <div class="megamenu">-->
<!--                                <ul class="single-mega cn-col-4">-->
<!--                                    <li class="title">Women's Collection</li>-->
<!--                                    <li><a href="/shop">Dresses</a></li>-->
<!--                                    <li><a href="/shop">Blouses &amp; Shirts</a></li>-->
<!--                                    <li><a href="/shop">T-shirts</a></li>-->
<!--                                    <li><a href="/shop">Rompers</a></li>-->
<!--                                    <li><a href="/shop">Bras &amp; Panties</a></li>-->
<!--                                </ul>-->
<!--                                <ul class="single-mega cn-col-4">-->
<!--                                    <li class="title">Men's Collection</li>-->
<!--                                    <li><a href="/shop">T-Shirts</a></li>-->
<!--                                    <li><a href="/shop">Polo</a></li>-->
<!--                                    <li><a href="/shop">Shirts</a></li>-->
<!--                                    <li><a href="/shop">Jackets</a></li>-->
<!--                                    <li><a href="/shop">Trench</a></li>-->
<!--                                </ul>-->
<!--                                <ul class="single-mega cn-col-4">-->
<!--                                    <li class="title">Kid's Collection</li>-->
<!--                                    <li><a href="/shop">Dresses</a></li>-->
<!--                                    <li><a href="/shop">Shirts</a></li>-->
<!--                                    <li><a href="/shop">T-shirts</a></li>-->
<!--                                    <li><a href="/shop">Jackets</a></li>-->
<!--                                    <li><a href="/shop">Trench</a></li>-->
<!--                                </ul>-->
<!--                                <div class="single-mega cn-col-4">-->
<!--                                    <img src="/siteAssets/img/bg-img/bg-6.jpg" alt="">-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </li>-->
<!--                        <li><a href="#">Pages</a>-->
<!--                            <ul class="dropdown">-->
<!--                                <li><a href="/">Home</a></li>-->
<!--                                <li><a href="/shop">Shop</a></li>-->
<!--                                <li><a href="/product-details">Product Details</a></li>-->
<!--                                <li><a href="/checkout">Checkout</a></li>-->
<!--                                <li><a href="/blog">Blog</a></li>-->
<!--                                <li><a href="/single-blog">Single Blog</a></li>-->
<!--                                <li><a href="/regular-page">Regular Page</a></li>-->
<!--                                <li><a href="/contact">Contact</a></li>-->
<!--                            </ul>-->
<!--                        </li>-->
<!--                        <li><a href="/blog">Blog</a></li>-->
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Admin</a>
                            <ul class="dropdown">
                                <li><a href="<?= Url::to(['/admin/product']) ?>">Products</a></li>
                                <li><a href="<?= Url::to(['/admin/images']) ?>">Images</a></li>
                                <li><a href="<?= Url::to(['/admin/category']) ?>">category</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- Nav End -->
            </div>
        </nav>

        <!-- Header Meta Data -->
        <div class="header-meta d-flex clearfix justify-content-end">
            <!-- Search Area -->
            <div class="search-area">
                <form action="#" method="post">
                    <input type="search" name="search" id="headerSearch" placeholder="Type for search">
                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                </form>
            </div>
            <!-- Favourite Area -->
            <div class="favourite-area">
                <a href="#"><img src="/siteAssets/img/core-img/heart.svg" alt=""></a>
            </div>
            <!-- User Login Info -->
            <?php if (Yii::$app->user->isGuest): ?>
                <div class="user-login-info">
                    <a href="<?= Url::toRoute(['auth/signup']) ?>">Register</a>
                </div>
                <div class="user-login-info">
                    <a href="<?= Url::toRoute(['auth/login']) ?>">Login</a>
                </div>
            <?php else: ?>
                <div class="user-login-info">
                    <a href="/person"><img src="/siteAssets/img/core-img/user.svg" alt=""></a>
                </div>
                <div class="user-login-info">
                    <a href="">
                    <?= Html::beginForm(['/auth/logout'], 'post') .
                    Html::submitButton(
                        'Logout',
                        ['class' => 'logout']
                    ) . Html::endForm() ?>
                    </a>
                </div>
            <?php endif; ?>
            <!-- Cart Area -->
            <div class="cart-area">
                <a href="#" id="essenceCartBtn"><img src="/siteAssets/img/core-img/bag.svg" alt=""> <span>3</span></a>
            </div>
        </div>

    </div>
</header>
<!-- ##### Header Area End ##### -->


<!-- ##### Right Side Cart End ##### -->




        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>



<?php $this->endBody() ?>

<!-- ##### Footer Area Start ##### -->
<footer class="footer_area clearfix">
    <div class="container">
        <div class="row">
            <!-- Single Widget Area -->
            <div class="col-12 col-md-6">
                <div class="single_widget_area d-flex mb-30">
                    <!-- Logo -->
                    <div class="footer-logo mr-50">
                        <a href="#"><img src="/siteAssets/img/core-img/logo2.png" alt=""></a>
                    </div>
                    <!-- Footer Menu -->
                    <div class="footer_menu">
                        <ul>
                            <li><a href="/shop">Shop</a></li>
                            <li><a href="/blog">Blog</a></li>
                            <li><a href="/contact">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Single Widget Area -->
            <div class="col-12 col-md-6">
                <div class="single_widget_area mb-30">
                    <ul class="footer_widget_menu">
                        <li><a href="#">Order Status</a></li>
                        <li><a href="#">Payment Options</a></li>
                        <li><a href="#">Shipping and Delivery</a></li>
                        <li><a href="#">Guides</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms of Use</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row align-items-end">
            <!-- Single Widget Area -->
            <div class="col-12 col-md-6">
                <div class="single_widget_area">
                    <div class="footer_heading mb-30">
                        <h6>Subscribe</h6>
                    </div>
                    <div class="subscribtion_form">
                        <form action="#" method="post">
                            <input type="email" name="mail" class="mail" placeholder="Your email here">
                            <button type="submit" class="submit"><i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Single Widget Area -->
            <div class="col-12 col-md-6">
                <div class="single_widget_area">
                    <div class="footer_social_area">
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                        <a href="#" data-toggle="tooltip" data-placement="top" title="Youtube"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-12 text-center">
                <p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <br>
                    Version : <?= \app\controllers\SiteController::getVersion() ?>
                </p>
            </div>
        </div>

    </div>
</footer>
<!-- ##### Footer Area End ##### -->

</body>
</html>
<?php $this->endPage() ?>
