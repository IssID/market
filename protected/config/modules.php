<?php
return [
    // подключаем модули
    'admin' => [
        'class' => 'app\modules\admin\Module',
    ],
    'roundSwitch' => [
        'class' => 'nickdenry\grid\toggle\Module',
    ],
];
